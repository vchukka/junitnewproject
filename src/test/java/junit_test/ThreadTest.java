package junit_test;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.Threads;

public class ThreadTest {

	@Test
	public void test() {
		Threads t1 = new Threads();
		Thread t2 = new Thread(t1,"t2");
		Thread t3 = new Thread(t2,"t3");
		Thread t4 = new Thread(t3,"t4");
		
		t2.setPriority(Thread.MAX_PRIORITY);
		t3.setPriority(Thread.MIN_PRIORITY);
		t4.setPriority(Thread.NORM_PRIORITY);
		System.out.println(t2);
		System.out.println(t3);
		System.out.println(t4);
		
		t2.start();
		t3.start();
		t4.start();
	}

}
