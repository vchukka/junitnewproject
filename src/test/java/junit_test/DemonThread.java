package junit_test;

import static org.junit.Assert.*;


import org.junit.Test;

import junit.DaemonThread;

public class DemonThread {

	@Test
	public void test() {
		DaemonThread dt = new DaemonThread();
		dt.setDaemon(true);
		dt.start();
		 assertEquals(false, DaemonThread.currentThread().isDaemon());
	     assertEquals(true, dt.isDaemon());
	}

	

}
