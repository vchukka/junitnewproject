package junit_test;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.Static;

public class Static_test {

	@Test
	public void nonstatictest() {
		Static test = new Static();
		System.out.println(test.nonstaticcount);
		assertNotEquals(3,test.nonstaticcount);
		
	}
	public void statics() {
		Static test = new Static();
		System.out.println(test.count);
		assertNotEquals(2,test.count);
	}

}
