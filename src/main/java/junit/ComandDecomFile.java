package junit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;

public class ComandDecomFile {
	public void file(String file1,String file2) throws IOException {
		try {
			FileInputStream fin = new FileInputStream(file1);
			FileOutputStream fout= new FileOutputStream(file2);
			DeflaterOutputStream dos = new DeflaterOutputStream(fout);
			
			int d;
			while((d = fin.read())!=-1){
				dos.write(d);
				
			}
			fin.close();
			dos.close();
			
		} catch (FileNotFoundException e) {
						e.printStackTrace();
		}
	}

}
