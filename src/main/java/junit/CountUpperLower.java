package junit;


	public class CountUpperLower {
		   
		public void count(String str) {
		      int upperCase = 0;
		      int lowerCase = 0;
		      char[] ch = str.toCharArray();
		      {
		      for(char chh : ch) {
		         if(chh >='A' && chh <='Z') {
		            upperCase++;
		         } else if (chh >= 'a' && chh <= 'z') {
		            lowerCase++;
		         } else {
		            continue;
		         }
		      }
		      System.out.println("Count of Uppercase letter/s is/are " + upperCase + " and of Lowercase       letter/s is/are " + lowerCase);
		   }
		}
	}


