package junit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;

public class StreamMf {
	
	
	
	public void stream() throws IOException {
		InputStream file1 = new FileInputStream("C:\\Users\\vchukka\\Desktop\\springboot student");
		InputStream file2 = new FileInputStream("C:\\Users\\vchukka\\Desktop\\springboot student");
		
		InputStream stm = new SequenceInputStream(file1,file2);
		
		System.out.println("Read data from "+file1+" & "
                +file2 +" using SequenceInputStream");
		 
        int ch;
        try {
			while ((ch = stm.read()) != -1) { 
			       System.out.print((char) ch);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		stm.close();
	}

}

