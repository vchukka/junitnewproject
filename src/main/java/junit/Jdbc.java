package junit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Jdbc {

	String DB_url = "jdbc:mysql://localhost/myspace";
	String userName = "root";
	String password = "root";

	public void jdbc() {

		try (Connection con = DriverManager.getConnection(DB_url, userName, password);
				Statement stm = con.createStatement();

) {
			String sql = "select found_rows()";
			stm.execute(sql);
			System.out.println("query executes..");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
